let btnActivate = document.getElementById("btnActivate");
btnActivate.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  activateInterval(tab);
});

let btnDeactivate = document.getElementById("btnDeactivate");
btnDeactivate.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  deactivateInterval(tab);
});

function activateInterval(tab) {
  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: execTabActivateInterval,
  });
}

function deactivateInterval(tab) {
  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: execTabDeactivateInterval,
  });  
}


function execTabActivateInterval() {
  chrome.storage.local.get("interval", function(result) {
    clearInterval(result.interval);
    let interval = window.setInterval(function(){
      let targetScrollY = window.scrollY + 400;
      window.scrollTo({
        top: targetScrollY,
        left: 0,
        behavior: 'smooth'
      });
      
    }, 6000);
    chrome.storage.local.set({ interval });
  });
}

function execTabDeactivateInterval() {
  chrome.storage.local.get("interval", function(result) {
    clearInterval(result.interval);
    chrome.storage.local.set({ interval: null });
  });
}
